const { exec } = require('child_process');
let command = exec('shutdown -s -t 00', function (err, stdout, stderr) {
  if (err || stderr) {
    console.log("shutdown failed" + err + stderr);
  }
});
command.stdin.end();
command.on('close', function (code) {
  console.log("shutdown", code);
});