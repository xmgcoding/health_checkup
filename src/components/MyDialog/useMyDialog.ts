import { reactive } from "vue";

type IModelProps = {
  title?: string;
  visible: boolean;
  showCancelBtn?: boolean;
  showConfirmBtn?: boolean;
  handleConfirmClick?: Function;
};
export default (options: IModelProps = { visible: false }): IModelProps => {
  const dialogConfig = reactive(options);
  return dialogConfig;
};
