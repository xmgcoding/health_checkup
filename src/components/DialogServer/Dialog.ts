import { App, createVNode, render } from 'vue'
import dialog from './Dialog.vue'

const divDom = document.createElement('div')
divDom.setAttribute('class', 'dialog-container')
document.body.appendChild(divDom);

const installDialogPlugin = (app: App) => {
    return (options: any) => {
        return dialogPlugin(options, app)
    }

}
const dialogPlugin = (options: any, app: App) => {
    return new Promise((resolve, reject) => {
        const div = document.createElement("div")
        divDom.appendChild(div);
        const okButton = async (children: any) => {
            debugger
            const flag = await children.handleOk()
            if (flag) {
                divDom.removeChild(div);
                resolve(true)
            }
        }
        const noButton = () => {
            divDom.removeChild(div);
            reject(new Error("取消"))
        }
        const vNode = createVNode(dialog, { ...options, okButton, noButton }, options.children)
        vNode.appContext = app._context
        render(vNode, div)
    })
}
export default {
    install(app: App) {
        app.config.globalProperties.$dialog = installDialogPlugin(app)
    }
}
