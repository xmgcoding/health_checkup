import CardBox from "./CardBox/index.vue";
import MyDialog from "./MyDialog/MyDialog.vue";

import CardReader from "./CardReader/index.vue";
import UserCard from "./UserCard/index.vue";

import { App } from "vue";

const components = [CardBox, MyDialog, CardReader, UserCard];
const install = (app: App): void => {
  components.map((item) => {
    return app.component(item.name, item);
  });
};

export { CardBox, MyDialog };
export default {
  install,
};
