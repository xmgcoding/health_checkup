import { App } from "vue";
import Vue3ProTable from "@/components/TablePro/index.vue";
export function loadV3ProTable(app: App) {
  /** 注册所有 Element Plus Icon */
  app.component("vue3-pro-table", Vue3ProTable);
}
