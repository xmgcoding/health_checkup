export interface IA00_04
  extends Partial<IBaseInfo>,
    Partial<ILifeStyle>,
    Partial<ICheckBody>,
    Partial<IAuxiliaryCheck>,
    Partial<IOrganFunction>,
    Partial<IExistIssue>,
    Partial<IHealthEvaluation>,
    Partial<ITreatmentCondition> {
  /**运动功能 */
  a00_04_64: string;
  id?: number;
}

export interface IHealthArchive
  extends Partial<IBaseInfo>,
    Partial<ILifeStyle>,
    Partial<ICheckBody>,
    Partial<IAuxiliaryCheck>,
    Partial<IOrganFunction>,
    Partial<IExistIssue>,
    Partial<IHealthEvaluation>,
    Partial<ITreatmentCondition> {
  a00_04_64: string;
}

/**基本信息*/
export interface IBaseInfo {
  //身份证号
  id_no: string;
  //姓名
  a00_03_03: string;
  //健康档案号
  a00_03_05: string;
  /**个人ID */
  a00_04_01: string;
  /**地址编码 */
  a00_04_02: string;
  /** 体检日期*/
  a00_04_03: string;
  /** 责任医生 */
  a00_04_04: string;
  /**症状 */
  a00_04_05: Array<string>;
  /** 症状-其他*/
  a00_04_06: string;
  /**体温 */
  a00_04_07: number | undefined;
  /**脉率 */
  a00_04_08: number | undefined;
  /**呼吸频率 */
  a00_04_09: number | undefined;
  /**血压-左侧-高 */
  a00_04_10: number | undefined;
  /**血压-左侧-低 */
  a00_04_11: number | undefined;
  /**血压-右侧-高 */
  a00_04_12: number | undefined;
  /**血压-右侧-低 */
  a00_04_13: number | undefined;
  /**身高 */
  a00_04_14: number | undefined;
  /**体重 */
  a00_04_15: number | undefined;
  /**腰围 */
  a00_04_16: number | undefined;
  /**体重指数 */
  a00_04_17: number | undefined;
  /**老年人 健康状态自我评估*/
  a00_04_184: string;
  /**老年人  自理 */
  a00_04_205: string;
  /**老年人 认知能力 */
  a00_04_20: string;
  /**老年人 简单智力状态检查总分 */
  a00_04_21: number | undefined;
  /**老年人 感情状态 */
  a00_04_22: string;
  /**老年人 老年人抑郁分检查总分 */
  a00_04_23: number | undefined;
}

/**生活方式*/
export interface ILifeStyle {
  /**生活方式 */
  /**体育锻炼 */
  /**锻炼频率 */
  a00_04_24: string;
  /**每次锻炼时间 分钟,整数，最长不超过5位*/
  a00_04_25: number | undefined;
  /**坚持锻炼时间 年,浮点型*/
  a00_04_26: number | undefined;
  /**锻炼方式 锻炼方式最多输入15个字符*/
  a00_04_27: string;
  /** 锻炼方式：额外记录的数据*/
  a00_04_27_v: Array<string>;
  /** 锻炼方式：额外记录的数据*/
  a00_04_27_s: string;
  /**饮食习惯 */
  a00_04_28: Array<string>;
  /**吸烟情况 */
  /**吸烟状况 1,2,3*/
  a00_04_29: string;
  /**平均 支/日*/
  a00_04_30: number | undefined;
  /**开始吸烟年龄 */
  a00_04_31: number | undefined;
  /**戒烟年龄 */
  a00_04_32: number | undefined;
  /**饮酒频率 */
  a00_04_33: string;
  /** 平均*/
  a00_04_34: number | undefined;
  /** 是否戒酒 */
  a00_04_35: string;
  /** 开始饮酒年龄 */
  a00_04_37: number | undefined;
  /** 戒酒年龄*/
  a00_04_36: number | undefined;
  /** 近一年内是否曾醉酒*/
  a00_04_38: string;
  /** 饮酒种类*/
  a00_04_39: Array<string>;
  /** 饮酒品种：其他*/
  a00_04_40: string;
  /** 职业病危害接触史：有无*/
  a00_04_41: string;
  /** 具体职业*/
  a00_04_42: string;
  /** 从业时间*/
  a00_04_43: number | undefined;
  /** 有毒种类：化学物质*/
  a00_04_44: string;
  /** 有毒种类：化学物质--防护措施 有无*/
  a00_04_45: string;
  /**  有毒种类：化学物质--防护措施 有 具体措施*/
  a00_04_46: string;
  /** 有毒种类：粉尘*/
  a00_04_47: string;
  /** 有毒种类：粉尘--防护措施 有无*/
  a00_04_48: string;
  /**  有毒种类：粉尘--防护措施 有 具体措施*/
  a00_04_49: string;
  /** 有毒种类：放射物质*/
  a00_04_50: string;
  /** 有毒种类：放射物质--防护措施 有无*/
  a00_04_51: string;
  /**  有毒种类：放射物质--防护措施 有 具体措施*/
  a00_04_52: string;
  /** 有毒种类：物理因素*/
  a00_04_186: string;
  /** 有毒种类：物理因素--防护措施 有无*/
  a00_04_187: string;
  /**  有毒种类：物理因素--防护措施 有 具体措施*/
  a00_04_188: string;
  /** 有毒种类：其他*/
  a00_04_189: string;
  /** 有毒种类：其他--防护措施 有无*/
  a00_04_190: string;
  /**  有毒种类：其他--防护措施 有 具体措施*/
  a00_04_191: string;
}

/** 体察信息*/
export interface ICheckBody {
  /**眼底 */
  a00_04_135: string;
  /**眼底异常 */
  a00_04_136: string;
  /**皮肤 */
  a00_04_65: string;
  a00_04_66: string;
  /**巩膜 */
  a00_04_67: string;
  a00_04_68: string;
  /**淋巴结 */
  a00_04_69: string;
  a00_04_70: string;
  /**肺-桶状胸 */
  a00_04_71: string;
  a00_04_72: string;
  /**肺-呼吸音 */
  a00_04_73: string;
  a00_04_74: string;
  /**肺-罗音 */
  a00_04_75: string;
  a00_04_76: string;
  /**心脏-心率 */
  a00_04_77: number | undefined;
  a00_04_78: string;
  /** 心脏-杂音*/
  a00_04_79: string;
  a00_04_80: string;
  /**腹部-压痛*/
  a00_04_81: string;
  a00_04_82: string;
  /**腹部-包块*/
  a00_04_83: string;
  a00_04_84: string;
  /**腹部-肝大*/
  a00_04_85: string;
  a00_04_86: string;
  /**腹部-脾大*/
  a00_04_87: string;
  a00_04_88: string;
  /**腹部-移动性浑浊*/
  a00_04_89: string;
  a00_04_90: string;
  /**下肢水肿 */
  a00_04_91: string;
  /**足背动脉搏动 */
  a00_04_92: string;
  /**肛门指诊 */
  a00_04_93: string;
  a00_04_94: string;
  /**乳腺 */
  a00_04_95: Array<string>;
  a00_04_96: string;
  /**妇科 外阴 */
  a00_04_97: string;
  a00_04_98: string;
  /**妇科 外阴 */
  a00_04_99: string;
  a00_04_100: string;
  /**妇科 宫颈 */
  a00_04_101: string;
  a00_04_102: string;
  /**妇科 宫体 */
  a00_04_103: string;
  a00_04_104: string;
  /**妇科 附件 */
  a00_04_105: string;
  a00_04_106: string;
  /**其他 */
  a00_04_107: string;
}

/** 辅助检查*/
export interface IAuxiliaryCheck {
  /**空腹血糖 */
  a00_04_108: number | undefined;
  a00_04_192: string;
  /**血常规 血红蛋白*/
  a00_04_109: number | undefined;
  /**血常规 白细胞*/

  a00_04_110: number | undefined;
  /**血常规 血小板*/

  a00_04_111: number | undefined;
  /**血常规 其他*/
  a00_04_112: string;
  /**尿常规 尿蛋白 */
  a00_04_113: string;
  /**尿常规  尿糖 */

  a00_04_114: string;
  /**尿常规  尿酮体 */
  a00_04_115: string;
  /**尿常规  尿潜血 */
  a00_04_116: string;
  /**尿常规  其他 */
  a00_04_117: string;
  /**尿微量白蛋白 */
  a00_04_118: number | undefined;
  /**大便潜血*/
  a00_04_119: string;
  /**肝功能 */
  /**血清谷丙转氨酶 */
  a00_04_120: string;
  /**血清谷草转氨酶 */
  a00_04_121: string;
  /**白蛋白 */
  a00_04_122: string;
  /**总胆红素 */
  a00_04_123: string;
  /**结合胆红素 */
  a00_04_124: string;
  /**肾功能 血清肌酐 */
  a00_04_125: number | undefined;
  /**肾功能 血尿素氮 */
  a00_04_126: number | undefined;
  /**肾功能 血钾浓度 */
  a00_04_127: number | undefined;
  /**肾功能 血钠浓度 */
  a00_04_128: number | undefined;
  /**血脂 总胆固醇  */
  a00_04_129: number | undefined;
  /**血脂 甘油三酯  */
  a00_04_130: number | undefined;
  /**血脂 血清低密度脂蛋白胆固醇  */
  a00_04_131: number | undefined;
  /**血脂 血清高密度脂蛋白胆固醇 */
  a00_04_132: number | undefined;
  /**糖化血红蛋白 */
  a00_04_133: number | undefined;
  /**  乙型肝炎表面抗原 */
  a00_04_134: number | undefined | any;

  /**心电图*/
  a00_04_137: string;
  /**心电图 异常*/
  a00_04_138: string;
  /**胸部X线片*/
  a00_04_139: string;
  /**胸部X线片 异常*/
  a00_04_140: string;

  /**B超*/
  a00_04_141: string;
  /**B超 异常*/
  a00_04_142: string;
  /**腹部B超*/
  a00_04_141f: string;
  /**腹部B超 异常*/
  a00_04_142f: string;
  /**宫颈涂片*/
  a00_04_143: string;
  /**宫颈涂片 异常*/
  a00_04_144: string;
  /**宫颈涂片 其他 */
  a00_04_145: string;
}

/** 脏器检查*/
export interface IOrganFunction {
  /** 听力*/
  a00_04_63: string;
  /**口唇*/
  a00_04_53: string;
  /**齿列 正常*/
  a00_04_54: string;
  /**齿列 缺齿 */
  a00_04_55: string;
  /**齿列 龋齿 */
  a00_04_56: string;
  /**齿列 义齿 */
  a00_04_57: string;
  /**齿列  缺齿  选项左上*/
  a00_04_193_1: Array<string>;
  /**齿列  缺齿  选项右上*/
  a00_04_193_2: Array<string>;
  /**齿列  缺齿  选项左下*/
  a00_04_193_3: Array<string>;
  /**齿列  缺齿  选项右下*/
  a00_04_193_4: Array<string>;
  /**齿列  龋齿  选项左上*/
  a00_04_194_1: Array<string>;
  /**齿列  龋齿  选项右上*/
  a00_04_194_2: Array<string>;
  /**齿列  龋齿  选项左下*/
  a00_04_194_3: Array<string>;
  /**齿列  龋齿  选项右下*/
  a00_04_194_4: Array<string>;
  /**齿列  义齿  选项左上*/
  a00_04_195_1: Array<string>;
  /**齿列  义齿  选项右上*/
  a00_04_195_2: Array<string>;
  /**齿列  义齿  选项左下*/
  a00_04_195_3: Array<string>;
  /**齿列  义齿  选项右下*/
  a00_04_195_4: Array<string>;
  /**口腔  咽部 */
  a00_04_58: string;
  /**视力 左眼 */
  a00_04_59: string;
  /**视力 右眼 */
  a00_04_60: string;
  /**视力  矫正视力 左眼 */
  a00_04_61: string;
  /**视力  矫正视力  右眼 */
  a00_04_62: string;
  /**听力 */
}

/**健康问题*/
export interface IExistIssue {
  /**脑血管疾病 */
  a00_04_155: Array<string>;
  /**脑血管疾病 其他*/
  a00_04_156: string;

  /**肾脏疾病 */
  a00_04_157: Array<string>;
  /**肾脏疾病 其他*/
  a00_04_158: string;
  /**心脏疾病 */
  a00_04_159: Array<string>;
  /**心脏疾病 其他*/
  a00_04_160: string;
  /**血管疾病 */
  a00_04_161: Array<string>;
  /**血管疾病 其他*/
  a00_04_162: string;
  /**眼部疾病 */
  a00_04_163: Array<string>;
  /**眼部疾病 其他*/
  a00_04_164: string;
  /**神经系统疾病   */
  a00_04_165: string;
  /**神经系统疾病   其他*/
  a00_04_166: string;
  /**其他系统疾病     */
  a00_04_167: string;
  /**其他系统疾病     其他*/
  a00_04_168: string;
}

/**健康评价*/
export interface IHealthEvaluation {
  /**健康评价 体检有误异常*/
  a00_04_169: string;
  /**健康评价异常一*/
  a00_04_170: string;
  /**健康评价异常二*/
  a00_04_171: string;
  /**健康评价异常三*/
  a00_04_172: string;
  /**健康评价异常四*/
  a00_04_173: string;
  /**健康评价异常五*/
  a00_04_203: string;
  /**健康评价异常六*/
  a00_04_204: string;
  /**健康评价其他异常*/
  a00_04_219: string;
  /** 非免疫规划预防接种史*/
  nonImmunization: Array<INonImmunizationRecord>;
  /**健康指导 */
  a00_04_174: Array<string>;
  /**体检类型 */
  a00_04_215: string;
  /**危险因素控制 */
  a00_04_175: Array<string>;
  a00_04_176: string;
  a00_04_177: string;
  a00_04_178: string;
}

export interface ITreatmentCondition {
  hospitalHistory: Array<IHospitalHistoryRecord>;
  familyHospitalBed: Array<IFamilyHospitalBedRecord>;
  medicationSituation: Array<IMedicationSituationRecord>;
}

/** 住院历史记录*/
export interface IHospitalHistoryRecord {
  a00_041_03: string;
  a00_041_04: string;
  a00_041_05: string;
  a00_041_06: string;
  a00_041_07: string;
}

/** 家庭病床历史记录*/
export interface IFamilyHospitalBedRecord {
  a00_042_03: string;
  a00_042_04: string;
  a00_042_05: string;
  a00_042_06: string;
  a00_042_07: string;
}

/** 主要用药历史记录*/
export interface IMedicationSituationRecord {
  a00_043_03: string;
  a00_043_04: string;
  a00_043_05: string;
  a00_043_06: string;
  a00_043_07: string;
  a00_043_09: string;
}

/** 非免疫规划预防接种史*/
export interface INonImmunizationRecord {
  a00_044_03: string;
  a00_044_04: string;
  a00_044_05: string;
}

// 终端公共模型
export interface ITerminalBase {
  id: string;
  //是否同步 0 未同步  1已同步
  synchronous: number;
  // 二维码
  qr_code_id: number | undefined;
  // 性别
  sex: string;
  // 本地ID
  data_id: string;
  /**身份证 */
  cardId: string;
  /**用户名 */
  userName: string;
  /**项目 */
  article: string;
  measureTime: number | undefined;
}
/**
 * 终端体检问询
 */
export interface IHealthArchiveCloud
  extends Partial<IHealthArchive>,
    Partial<ITerminalBase> {}
/**
 * 终端中医问询模型
 */
export interface ITerminalB04_53 extends IB04_53, ITerminalBase {}

/**
 * web端中医问询模型
 */
export interface IB04_53 {
  eid: string;
  xqbm: string;
  zxjg: string;
  qyid: string;
  qytid: string;
  ryid: string;
  qygrid: string;
  srcode: string;
  a00_03_03: string;
  a00_04_00: string;
  b04_053_01: string;
  b04_053_02: string;
  b04_053_03: string;
  b04_053_04: string;
  b04_053_45: string;
  b04_053_08_1: number | undefined;
  b04_053_08_2: number | undefined;
  b04_053_08_3: number | undefined;
  b04_053_08_4: number | undefined;
  b04_053_08_5: number | undefined;
  b04_053_08_6: number | undefined;
  b04_053_08_7: number | undefined;
  b04_053_08_8: number | undefined;
  b04_053_08_9: number | undefined;
  b04_053_08_10: number | undefined;
  b04_053_08_11: number | undefined;
  b04_053_08_12: number | undefined;
  b04_053_08_13: number | undefined;
  b04_053_08_14: number | undefined;
  b04_053_08_15: number | undefined;
  b04_053_08_16: number | undefined;
  b04_053_08_17: number | undefined;
  b04_053_08_18: number | undefined;
  b04_053_08_19: number | undefined;
  b04_053_08_20: number | undefined;
  b04_053_08_21: number | undefined;
  b04_053_08_22: number | undefined;
  b04_053_08_23: number | undefined;
  b04_053_08_24: number | undefined;
  b04_053_08_25: number | undefined;
  b04_053_08_26: number | undefined;
  b04_053_08_27: number | undefined;
  b04_053_08_28: number | undefined;
  b04_053_08_29: number | undefined;
  b04_053_08_30: number | undefined;
  b04_053_08_31: number | undefined;
  b04_053_08_32: number | undefined;
  b04_053_08_33: number | undefined;
  b04_053_09: number | undefined;
  b04_053_18: string;
  b04_053_10: number | undefined;
  b04_053_19: string;
  b04_053_11: number | undefined;
  b04_053_20: string;
  b04_053_12: number | undefined;
  b04_053_21: string;
  b04_053_13: number | undefined;
  b04_053_22: string;
  b04_053_14: number | undefined;
  b04_053_23: string;
  b04_053_15: number | undefined;
  b04_053_24: string;
  b04_053_16: number | undefined;
  b04_053_25: string;
  b04_053_17: number | undefined;
  b04_053_26: string;
  b04_053_27: string[];
  b04_053_35: string[];
  b04_053_36: string;
  b04_053_28: string[];
  b04_053_37: string;
  b04_053_29: string[];
  b04_053_38: string;
  b04_053_30: string[];
  b04_053_39: string;
  b04_053_31: string[];
  b04_053_40: string;
  b04_053_32: string[];
  b04_053_41: string;
  b04_053_33: string[];
  b04_053_42: string;
  b04_053_34: string[];
  b04_053_43: string;
  b04_053_44: string;
}
