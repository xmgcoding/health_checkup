//家庭
export interface IA00_02 {
  //家庭档案编号
  key: string
  //身份证号
  id_no: string
  //行政区号
  area_no: string
  // 省份
  a00_02_93: string
  // 市
  a00_02_94: string
  //区县
  a00_02_95: string
  //乡镇
  a00_02_96: string
  //社区
  a00_02_97: string
  // 地址
  a00_02_13: string
  // 家庭卡号
  a00_02_100: string
  // 家庭电话
  a00_02_119: string
  // 医疗证号
  a00_02_31: string
  // 家庭电话
  a00_02_10: string
  // 厨房排风设施
  a00_02_111: string
  // 燃料类型
  a00_02_112: string
  // 饮水
  a00_02_113: string
  // 厕所
  a00_02_114: string
  // 禽畜栏
  a00_02_115: string
  // 居住面积
  a00_02_14: number | undefined
  // 住房间数
  a00_02_15: number | undefined
  // 厨房面积
  a00_02_18: number | undefined
  // 厨房间数
  a00_02_19: number | undefined
  // 饮用水来源：
  a00_02_16: string
  // 厨房类型
  a00_02_20: string
  //厕所
  a00_02_22: string
  // 经济情况：
  a00_02_24: number | undefined
  a00_02_25: number | undefined
  a00_02_26: number | undefined
  a00_02_27: number | undefined
  // 未成年子女数
  a00_02_35: number | undefined
  // 负担老人数
  a00_02_36: number | undefined
  // 责任医生ID
  a00_02_39: string
  // 责任医生姓名
  a00_02_91: string
  // 建档时间
  a00_02_29: string
  // 服务机构
  a00_02_92: string
  // 建档人
  a00_02_90: string
  // 备注
  a00_02_37: string
  // 排序
  a00_02_28: number | undefined
  a00_02_00: string
  a00_02_01: string
  a00_02_04: string
  a00_02_05: string
  a00_02_06: string
  a00_02_07: string
  a00_02_99: string
  a00_02_08: string
  a00_02_09: string
  a00_02_40: string
  a00_02_41: string
  a00_02_42: string
  a00_02_43: string
  a00_02_60: string
}
