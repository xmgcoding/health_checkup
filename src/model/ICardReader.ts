export interface ICardReaderResult {
  name: string
  resultFlag: number
  errorMsg: string
  //  结果信息
  resultContent: ICardReaderContent
}

export interface ICardReaderContent {
  //中国居民身份证：1；外国永久居留身份证：50; 台湾居住证：54；港澳居住证：55；
  certType: number
  //姓名
  partyName: string
  //性别  男：1；女：0
  gender: number
  //民族/国籍
  nation: string
  //出生日期 yyyyMMdd
  bornDay: string
  //住址
  certAddress: string
  //身份证号
  certNumber: string
  //签发机关
  certOrg: string
  //开始期限
  effDate: string
  //结束期限
  expDate: string

  //身份证照片
  identityPic: string
  //身份证正面照片
  identityFrontPic: string
  //身份证背面照片
  identityBackPic: string
  //身份证正反面照片
  identityPrintPic: string
}
