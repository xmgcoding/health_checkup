import {IBloodPressCloud, Ihw700Cloud} from "@/model/TerminalType";

/**血压记录 */

export interface IPagination {
  currentPage: number;
  pageSize: number;
  total: number;
}

export interface IBloodPressureTableProps {
  tableData?: Array<IBloodPressCloud>;
  pagination: IPagination;
}
export interface IHW700TableProps {
  tableData?: Array<Ihw700Cloud>;
  pagination: IPagination;
}

export interface IBluetoothDesc {
  type: "success" | "warning" | "info" | "error";
  info: string;
}

export interface ISearchParams {
  measureTime: [number, number];
  name: string;
}

export interface ICheckArticle {
  name: string;
  id: string;
  type: "" | "success" | "warning" | "info" | "danger";
}

export interface IBluetooth {
  deviceId: string;
  deviceName: string;
}


