// 人员档案表
/**IA00_03 */

export interface IA00_03 {
  id?: string
  //健康档案号
  a00_03_03: string
  a00_03_05: string
  a00_03_08: string
  organizeCode: string
  //编号
  a00_03_04: string
  a00_03_93: string
  a00_03_94: string
  a00_03_95: string
  a00_03_96: string
  a00_03_97: string
  a00_03_99: string
  institution_id: string
  // 村【社区】ID
  a00_03_70: string
  a00_03_07: string
  a00_03_10: string
  // 出生日期
  a00_03_09: string
  // 本人电话
  a00_03_12: string
  a00_03_11: string
  a00_03_13: string
  a00_03_14: string
  a00_03_133: string
  a00_03_134: string
  a00_03_15: number | undefined
  a00_03_16: string
  a00_03_17: string
  a00_03_18: number | undefined
  a00_03_19: number | undefined
  a00_03_20: number | undefined
  a00_03_21: number | undefined
  a00_03_22: string
  a00_03_23: number | undefined
  a00_03_54: string
  a00_03_24: Array<string>
  a00_03_25: string
  a00_03_26: Array<string>
  a00_03_27: string
  a00_03_69: Array<string>
  diseases: string
  surgeries: string
  traumas: string
  transfusions: string
  a00_03_28: Array<string>
  a00_03_29: string
  a00_03_30: Array<string>
  a00_03_31: string
  a00_03_32: Array<string>
  a00_03_33: string
  a00_03_34: Array<string>
  a00_03_35: string
  a00_03_36: string
  a00_03_37: string
  a00_03_38: Array<string>
  a00_03_39: string
  a00_03_65: number | undefined
  a00_03_121: Array<string>
  a00_03_138: string
  a00_03_143: string
  a00_03_144: string
  a00_03_120: number | undefined
  a00_03_136: number | undefined
  a00_03_111: string
  a00_03_112: string
  a00_03_113: string
  a00_03_114: string
  a00_03_115: string
  a00_03_51: string
  a00_03_53: string
  a00_03_92: string
  a00_03_61: string
  a00_03_52: string
  // 联系人一关系
  a00_03_132: string
  // 联系人二关系
  a00_03_135: string
  // 现住址
  a00_03_116: string
  a00_031_07: string
  //疾病
  disease: Array<IDisease>
  //  手术
  surgery: Array<ISurgery>
  //  外伤
  trauma: Array<ITrauma>
  //输血
  transfuse: Array<ITransfuse>
  //关联家庭
  family_id: string
  a00_031_10: string
  a00_031_13: string
  // 高血压：是|否
  gao: string,
  // 糖尿病：是|否
  tang: string,
}

export interface IDisease {
  state?: boolean
  date?: string
  code?: string
  name?: string
  aliasName?: string
  hasAlias?: boolean
  hasDate?: boolean
}

//手术
export interface ISurgery {
  name?: string
  date?: string
}

//外伤
export interface ITrauma {
  name?: string
  date?: string
}

//输血
export interface ITransfuse {
  name?: string
  date?: string
}
