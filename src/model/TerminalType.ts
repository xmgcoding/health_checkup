import {ITerminalBase} from "./IA00_04";

/**
 * 血液信息
 * NormalResult  血球分析表   Testinfo 用户表   TestResult  用户对应的表
 *
 * select * from TestResult
 left join Testinfo on  TestInfo.id=TestResult.TestInfo
 left join NormalResult on    TestResult.NormalResult=NormalResult.id
 where TestResult.Time>'2023-02-01' and TestResult.Time<='2023-02-02'  and TestResult.state=2
 order by TestResult.ID asc
 姓名 性别 时间 序号
 保存 TestResult+公共字段
 */

export interface IBiochemical {
  ID: string;
  Wic: string;
  Woc: string;
  Lyp: string;
  Mop: string;
  Neup: string;
  Eosp: string;
  Basop: string;
  Ly: string;
  Mo: string;
  Neu: string;
  Eos: string;
  Baso: string;
  /**红细胞计数 */
  Rbc: string;
  /**血红蛋白 */
  Hgb: string;
  Hct: string;
  /**平均红细胞体积 */
  Mcv: string;
  Mch: string;
  Mchc: string;
  Rdw: string;
  /**红细胞分布宽度标准差 */
  RdwSD: string;
  /**红细胞分布宽度变异系数 */
  RdwCV: string;
  /**血小板计数 */
  Plt: string;
  /**平均血小板体积 */
  Mpv: string;
  /**血小板分布宽度 */
  Pdw: string;
  Pct: string;
  Plcr: string;
  Plcc: string;
  // /**网织 红细胞 */
  // Retic: number;
  // /**网织红细胞指数 */
  // Irf: number;
  // 白细胞
  WbcTime: string;
  RbcTime: string;
  WocRealLength: string;
  Time: string;
  CRP: string;
  hsCRP: string;
  Channel: string;
  // 检验者
  checkDoctor: string;
  wbcMessage: string;
  rbcMessage: string;
  pltMessage: string;
}

export interface IBiochemicalCloud
  extends Partial<IBiochemical>,
    Partial<ITerminalBase> {
}

export const createBiochemicalCloud = (): IBiochemicalCloud => {
  return {
    pltMessage: "",
    rbcMessage: "",
    wbcMessage: "",
    checkDoctor: "",
    ID: "",
    Wic: "",
    Woc: "",
    Lyp: "",
    Mop: "",
    Neup: "",
    Eosp: "",
    Basop: "",
    Ly: "",
    Mo: "",
    Neu: "",
    Eos: "",
    Baso: "",
    Rbc: "",
    Hgb: "",
    Hct: "",
    Mcv: "",
    Mch: "",
    Mchc: "",
    Rdw: "",
    RdwSD: "",
    RdwCV: "",
    Plt: "",
    Mpv: "",
    Pdw: "",
    Pct: "",
    Plcr: "",
    Plcc: "",
    WbcTime: "",
    RbcTime: "",
    WocRealLength: "",
    Time: "",
    CRP: "",
    hsCRP: "",
    Channel: "",
    synchronous: 0,
    qr_code_id: undefined,
    sex: "",
    data_id: undefined,
    cardId: "",
    userName: "",
    article: "",
    measureTime: undefined,
  };
};
export const createBiochemistryCloud = (): IBiochemistryCloud => {
  return {
    Pat_Checker: "",
    Pat_Doctor: "",
    Pat_ID: "",
    Pat_Name: "",
    Pat_Report_Date: "",
    Pat_Sending_Date: "",
    Pat_Sex: 0,
    Pat_TestNo: "",
    Pat_Test_Time: "",
    Pat_Testing_Date: "",
    article: "",
    cardId: "",
    data_id: undefined,
    measureTime: undefined,
    qr_code_id: undefined,
    result: [],
    sex: "",
    synchronous: 0,
    userName: "",
  };
};

/**B超模型 */

export interface IBusView {
  // 截图 Base64 数组
  photos: Array<string>;
  // 超声结果
  result: Array<string>;
  // 医生 手填
  doctor: string;
  // 检查部位  选择
  checkPoint: string;
  // 巡检意见
  comment: string;
}

export interface IBusViewCloud
  extends Partial<IBusView>,
    Partial<ITerminalBase> {
}

// B超结果模型
export interface IBusResult {
  id?: number;
  title: string;
  content: string;
}

/**生化模型 */
export interface IBiochemistry {
  Pat_Name: string;
  // 1女 0男
  Pat_Sex: number;
  Pat_ID: string;
  Pat_TestNo: string;
  Pat_Sending_Date: string;
  Pat_Testing_Date: string;
  Pat_Report_Date: string;
  Pat_Test_Time: string;
  Pat_Doctor: string;
  Pat_Checker: string;
  result: Array<{
    // 检测箱简称
    TestName: string;
    // 检测箱中文
    TestCnName: string;
    // 检测结果
    TestResult: string;
    // 检测范围
    TestRange: string;
    // 单位
    TestUtil: string;
  }>;
}

export interface IBiochemistryCloud
  extends Partial<IBiochemistry>,
    Partial<ITerminalBase> {
}


export interface IBloodPress{
  /**收缩压 高压*/
  systolic?: number | undefined;
  /**舒张压   低压*/
  diastolic?: number | undefined;
  /**收缩压右 */
  systolicRight?: number | undefined;
  /**舒张压右 */
  diastolicRight?: number | undefined;
  /**心率 */
  heartRate?: number | undefined;
}

export interface IBloodPressCloud extends IBloodPress,ITerminalBase{}

// 
/**身高体重表 */
export interface Ihw700 {
  height: number;
  weight: number;
  bim: number;
  waistline: number;
}

/**
 * 身高体重
 */
export interface Ihw700Cloud extends Ihw700, ITerminalBase {
}

export const createHw700 = (): Ihw700Cloud => {
  return {
    article: "",
    bim: 0,
    cardId: "",
    data_id: "",
    height: 0,
    id: "",
    measureTime: undefined,
    qr_code_id: undefined,
    sex: "",
    synchronous: 0,
    userName: "",
    waistline: 0,
    weight: 0
  };
};
