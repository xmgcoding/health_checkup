export interface IQRCodeInfo {
  //
  id: number;
  username: string;
  sex: string;
  idno: string;
}

export const createQRCodeInfo = (): Partial<IQRCodeInfo> => {
  return {
    username: "",
    sex: "",
    idno: "",
  };
};
