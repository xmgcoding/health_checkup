// import { request } from "@/utils/service"
import { service, IPage } from "@/utils/service";
import { IA00_03, IA00_02 } from "@/model";

/**
 * 获取家庭信息列表
 */
export const getFamilyArchivesList = async (params) => {
  params.limit = params.pageSize;
  params.page = params.pageNum;
  const data = await service.request<IA00_02[]>({
    url: "/families",
    method: "get",
    params,
  });
  return data;
};
/**
 * 保存家庭信息
 * @param familyArchives
 */
export const saveFamilyArchives = (familyArchives) => {
  return service.request({
    url: "/families",
    method: "post",
    data: familyArchives.value,
  });
};

export const delFamilyArchives = (id) => {
  return service.request({
    url: "/families/" + id,
    method: "delete",
  });
};
/**
 * 获取家庭档案详情
 * @param cardId
 */
export const getFamilyArchiveById = async (cardId) => {
  const data = await service.request({
    url: "/families/" + cardId,
    method: "get",
  });
  return data;
};

export const checkPersonalArchive = async (cardId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const data = await service.request({
        url: "/profiles/" + cardId,
        method: "get",
      });
      resolve(true);
    } catch (e) {
      console.log(e);
      resolve(false);
    }
  });
};
/**
 * 获取个人档案详情
 */
export const getProfilesInfoByCardId = async (cardId) => {
  const data = await service.request<IA00_03>({
    url: "/profiles/" + cardId,
    method: "get",
  });
  return data;
};
/**删除个人档案 */
export const delProfilesInfoById = async (cardId) => {
  const data = await service.request({
    url: "/profiles/" + cardId,
    method: "delete",
  });
  return data;
};

export const saveProfiles = (profiles) => {
  return service.request({
    url: "/profiles",
    method: "post",
    data: profiles,
  });
};
// 检查个人档案是否存在
export const searchProfilesByNoId = (no_id) => {
  return service.request({
    url: "/profiles/searchByNoId",
    method: "get",
    params: {
      no_id,
    },
  });
};

export const getProfilesList = (params) => {
  params.limit = params.pageSize;
  params.page = params.pageNum;
  return service.request<IPage<Array<IA00_03>>>({
    url: "/profiles",
    method: "get",
    params,
  });
};

export const saveHealthArchives = (data) => {
  return service.request({
    url: "/health_examinations",
    method: "post",
    data,
  });
};
export const delHealthArchives = (id) => {
  return service.request({
    url: "/health_examinations/" + id,
    method: "delete",
  });
};
export const getHealthArchivesList = (params) => {
  params.limit = params.pageSize;
  params.page = params.pageNum;
  return service.request({
    url: "/health_examinations",
    method: "get",
    params,
  });
};
export const getHealthArchivesById = (id: string) => {
  return service.request({
    url: "/health_examinations/" + id,
    method: "get",
  });
};

// 保存二维码信息
export const saveQrCode = (data) => {
  return service.request({
    url: "/health_examination_qr_codes",
    method: "post",
    data,
  });
};
export const getQrList = async (data) => {
  return await service.request({
    url: "/health_examination_qr_codes",
    method: "get",
    params: data,
  });
};
