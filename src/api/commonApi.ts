import { service } from "@/utils/service";

export const getCommunityList = () => {
  return service.request({
    url: "/institutions",
    method: "get",
  }) as any;
};

export const getDoctorsList = () => {
  return service.request({
    url: "/doctors",
    method: "get",
  }) as any;
};
