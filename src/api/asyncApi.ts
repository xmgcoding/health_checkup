import { service } from "@/utils/service";

export const asyncHWData = async (data) => {
  const request = await service.request({
    url: "/height_weight_infos/batchAdd",
    data: data,
    method: "post",
  });
  return request;
};

export const asyncBusView = async (data) => {
  const result = await service.request({
    url: "/buses/batchAdd",
    data: data,
    method: "post",
  });
  return result;
};

/**
 * 中医体质
 */

export const asyncAgedTcm = async (data) => {
  const request = await service.request({
    url: "/health_examination_inquiries/batchAdd",
    data: data,
    method: "post",
  });
  return request;
};

export const asyncHealthCheck = async (data) => {
  const request = await service.request({
    url: "/health_examinations/batchAdd",
    data: data,
    method: "post",
  });
  return request;
};

export const asyncBloodPressure = async (data) => {
  const request = await service.request({
    url: "/blood_pressures/batchAdd",
    data: data,
    method: "post",
  });
  return request;
};

export const asyncBiochemical = async (data) => {
  const request = await service.request({
    url: "/blood_infos/batchAdd",
    data: data,
    method: "post",
  });
  return request;
};

export const asyncBiochemistry = async (data) => {
  return await service.request({
    url: "/biochemistries/batchAdd",
    data: data,
    method: "post",
  });
};
