import convert from "xml-js";

const result = convert.xml2json(`<?xml version="1.0" standalone="yes"?>
<NewDataSet>
  <Table1>
    <name>氢氯噻嗪</name>
    <yongfa>口服</yongfa>
    <jiliang>1</jiliang>
    <unit>片</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory>山东省济南市高新区</factory>
    <leixing>高血压</leixing>
    <szm>QLSQ</szm>
  </Table1>
  <Table1>
    <name>氯噻酮</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>LST</szm>
  </Table1>
  <Table1>
    <name>呋塞米</name>
    <yongfa>口服</yongfa>
    <jiliang>20</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>FSM</szm>
  </Table1>
  <Table1>
    <name>安体舒通</name>
    <yongfa>口服</yongfa>
    <jiliang>2</jiliang>
    <unit>片</unit>
    <cishu>每日2次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>ATST</szm>
  </Table1>
  <Table1>
    <name>氨苯蝶啶</name>
    <yongfa>口服</yongfa>
    <jiliang>2</jiliang>
    <unit>片</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>ABDD</szm>
  </Table1>
  <Table1>
    <name>美托洛尔</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>MTLE</szm>
  </Table1>
  <Table1>
    <name>阿替洛尔</name>
    <yongfa>口服</yongfa>
    <jiliang>25</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>ATLE</szm>
  </Table1>
  <Table1>
    <name>阿罗洛尔</name>
    <yongfa>口服</yongfa>
    <jiliang>10</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>ALLE</szm>
  </Table1>
  <Table1>
    <name>比索洛尔</name>
    <yongfa>口服</yongfa>
    <jiliang>5</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>BSLE</szm>
  </Table1>
  <Table1>
    <name>卡维地洛</name>
    <yongfa>口服</yongfa>
    <jiliang>25</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>KWDL</szm>
  </Table1>
  <Table1>
    <name>硝苯地平</name>
    <yongfa>口服</yongfa>
    <jiliang>10</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>XBDP</szm>
  </Table1>
  <Table1>
    <name>硝苯地平缓释片</name>
    <yongfa>口服</yongfa>
    <jiliang>20</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>XBDPHSP</szm>
  </Table1>
  <Table1>
    <name>尼群地平</name>
    <yongfa>口服</yongfa>
    <jiliang>10</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>NQDP</szm>
  </Table1>
  <Table1>
    <name>非洛地平</name>
    <yongfa>口服</yongfa>
    <jiliang>2.5</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>FLDP</szm>
  </Table1>
  <Table1>
    <name>氨洛地平</name>
    <yongfa>口服</yongfa>
    <jiliang>10</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>ALDP</szm>
  </Table1>
  <Table1>
    <name>乐息平</name>
    <yongfa>口服</yongfa>
    <jiliang>2</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>LXP</szm>
  </Table1>
  <Table1>
    <name>卡托普利</name>
    <yongfa>口服</yongfa>
    <jiliang>25</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>KTPL</szm>
  </Table1>
  <Table1>
    <name>依那普利</name>
    <yongfa>口服</yongfa>
    <jiliang>5</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>YNPL</szm>
  </Table1>
  <Table1>
    <name>贝那普利</name>
    <yongfa>口服</yongfa>
    <jiliang>10</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>BNPL</szm>
  </Table1>
  <Table1>
    <name>培哚普利</name>
    <yongfa>口服</yongfa>
    <jiliang>4</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>PDPL</szm>
  </Table1>
  <Table1>
    <name>赖诺普利</name>
    <yongfa>口服</yongfa>
    <jiliang>5～10</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>LNPL</szm>
  </Table1>
  <Table1>
    <name>西拉普利</name>
    <yongfa>口服</yongfa>
    <jiliang>2.5～5</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>XLPL</szm>
  </Table1>
  <Table1>
    <name>氯沙坦</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>LST</szm>
  </Table1>
  <Table1>
    <name>缬沙坦</name>
    <yongfa>口服</yongfa>
    <jiliang>80</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>胶囊</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>XST</szm>
  </Table1>
  <Table1>
    <name>伊贝沙坦</name>
    <yongfa>口服</yongfa>
    <jiliang>150</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>YBST</szm>
  </Table1>
  <Table1>
    <name>哌唑嗪</name>
    <yongfa>口服</yongfa>
    <jiliang>0.5～1</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>PZQ</szm>
  </Table1>
  <Table1>
    <name>特拉唑嗪</name>
    <yongfa>口服</yongfa>
    <jiliang>1～5</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>TLZQ</szm>
  </Table1>
  <Table1>
    <name>复方罗布麻片</name>
    <yongfa>口服</yongfa>
    <jiliang>2</jiliang>
    <unit>片</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>FFLBMP</szm>
  </Table1>
  <Table1>
    <name>珍菊降压片</name>
    <yongfa>口服</yongfa>
    <jiliang>1</jiliang>
    <unit>片</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>ZJJYP</szm>
  </Table1>
  <Table1>
    <name>复方丹参片</name>
    <yongfa>口服</yongfa>
    <jiliang>3</jiliang>
    <unit>片</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>高血压</leixing>
    <szm>FFDCP</szm>
  </Table1>
  <Table1>
    <name>格列本脲（优降糖）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>2.5</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>GLB*,YJT</szm>
  </Table1>
  <Table1>
    <name>格列吡嗪（美吡哒）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>2.5</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>GLBQ,MBD</szm>
  </Table1>
  <Table1>
    <name>格列齐特（达美康）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>80</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>GLQT,DMK</szm>
  </Table1>
  <Table1>
    <name>格列喹酮（糖适平）</name>
    <yongfa>早餐前口服</yongfa>
    <jiliang>30</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>GLKT,TSP</szm>
  </Table1>
  <Table1>
    <name>格列美脲（亚莫利）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>2</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>GLMN,YML</szm>
  </Table1>
  <Table1>
    <name>二甲双胍（降糖片）</name>
    <yongfa>餐中口服</yongfa>
    <jiliang>500</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>EJSG,JTP</szm>
  </Table1>
  <Table1>
    <name>苯乙双胍（降糖灵）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>50～200</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>BYSG,JTL</szm>
  </Table1>
  <Table1>
    <name>阿卡波糖（拜糖平）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>AKBT,BTP</szm>
  </Table1>
  <Table1>
    <name>伏格列波糖（倍欣）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>0.2</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>FGLBT,BX</szm>
  </Table1>
  <Table1>
    <name>罗格列酮(RSG)</name>
    <yongfa>早餐前口服</yongfa>
    <jiliang>4</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>LGLT,RSG</szm>
  </Table1>
  <Table1>
    <name>吡格列酮（艾汀）</name>
    <yongfa>早餐前口服</yongfa>
    <jiliang>15～30</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>每日1次</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>BGLT,AT</szm>
  </Table1>
  <Table1>
    <name>瑞格列奈（诺和龙）</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>4</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>RGLN,NHL</szm>
  </Table1>
  <Table1>
    <name>胰岛素</name>
    <yongfa>注射</yongfa>
    <jiliang>0.3～0.4</jiliang>
    <unit>U/KG</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>注射液</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>YDS</szm>
  </Table1>
  <Table1>
    <name>消渴丸</name>
    <yongfa>餐前口服</yongfa>
    <jiliang>5～10</jiliang>
    <unit>丸</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>糖尿病</leixing>
    <szm>XKW</szm>
  </Table1>
  <Table1>
    <name>氯丙嗪</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>LBQ</szm>
  </Table1>
  <Table1>
    <name>奋乃静</name>
    <yongfa>口服</yongfa>
    <jiliang>4～16</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>FNJ</szm>
  </Table1>
  <Table1>
    <name>氯普塞吨</name>
    <yongfa>口服</yongfa>
    <jiliang>25～50</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>LPSD</szm>
  </Table1>
  <Table1>
    <name>氟哌啶醇</name>
    <yongfa>口服</yongfa>
    <jiliang>12</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>FPDC</szm>
  </Table1>
  <Table1>
    <name>舒必利</name>
    <yongfa>口服</yongfa>
    <jiliang>0.1</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>SBL</szm>
  </Table1>
  <Table1>
    <name>氯氮平</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>LDP</szm>
  </Table1>
  <Table1>
    <name>奥氮平</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>ADP</szm>
  </Table1>
  <Table1>
    <name>喹硫平</name>
    <yongfa>口服</yongfa>
    <jiliang>600</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>KLP</szm>
  </Table1>
  <Table1>
    <name>利培酮(卓夫）</name>
    <yongfa>口服</yongfa>
    <jiliang>3</jiliang>
    <unit>mg</unit>
    <cishu>每日2次</cishu>
    <ci>2</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>LPT,ZF</szm>
  </Table1>
  <Table1>
    <name>阿立哌唑</name>
    <yongfa>口服</yongfa>
    <jiliang>10</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>ALPZ</szm>
  </Table1>
  <Table1>
    <name>地西泮</name>
    <yongfa>口服</yongfa>
    <jiliang>1</jiliang>
    <unit>片</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>DXB</szm>
  </Table1>
  <Table1>
    <name>氯硝西泮</name>
    <yongfa>口服</yongfa>
    <jiliang>2.5</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>LXXB</szm>
  </Table1>
  <Table1>
    <name>艾司唑仑</name>
    <yongfa>口服</yongfa>
    <jiliang>3</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>ASZL</szm>
  </Table1>
  <Table1>
    <name>阿普唑仑</name>
    <yongfa>口服</yongfa>
    <jiliang>0.4</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>APZL</szm>
  </Table1>
  <Table1>
    <name>佐匹克隆</name>
    <yongfa>口服</yongfa>
    <jiliang>3.75</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>ZPKL</szm>
  </Table1>
  <Table1>
    <name>异丙嗪</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>YBQ</szm>
  </Table1>
  <Table1>
    <name>氟西汀</name>
    <yongfa>口服</yongfa>
    <jiliang>20</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>FXT</szm>
  </Table1>
  <Table1>
    <name>帕罗西汀</name>
    <yongfa>口服</yongfa>
    <jiliang>5</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>PLXT</szm>
  </Table1>
  <Table1>
    <name>西酞普兰</name>
    <yongfa>口服</yongfa>
    <jiliang>20</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>XTPL</szm>
  </Table1>
  <Table1>
    <name>氟伏沙明</name>
    <yongfa>口服</yongfa>
    <jiliang>100～200</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>FFSM</szm>
  </Table1>
  <Table1>
    <name>舍曲林</name>
    <yongfa>口服</yongfa>
    <jiliang>50</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>SQL</szm>
  </Table1>
  <Table1>
    <name>阿米替林</name>
    <yongfa>口服</yongfa>
    <jiliang>25</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>AMTL</szm>
  </Table1>
  <Table1>
    <name>多虑平</name>
    <yongfa>口服</yongfa>
    <jiliang>25</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>DLP</szm>
  </Table1>
  <Table1>
    <name>丙咪嗪</name>
    <yongfa>口服</yongfa>
    <jiliang>12.5～25</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>BMQ</szm>
  </Table1>
  <Table1>
    <name>氯丙咪嗪</name>
    <yongfa>口服</yongfa>
    <jiliang>25</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>LBMQ</szm>
  </Table1>
  <Table1>
    <name>米氮平</name>
    <yongfa>口服</yongfa>
    <jiliang>15</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>MDP</szm>
  </Table1>
  <Table1>
    <name>曲唑酮</name>
    <yongfa>口服</yongfa>
    <jiliang>50/100/400</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>QZT</szm>
  </Table1>
  <Table1>
    <name>文拉法辛</name>
    <yongfa>口服</yongfa>
    <jiliang>150</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>WLFX</szm>
  </Table1>
  <Table1>
    <name>碳酸锂</name>
    <yongfa>口服</yongfa>
    <jiliang>600</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>TSL</szm>
  </Table1>
  <Table1>
    <name>丙戊酸钠</name>
    <yongfa>口服</yongfa>
    <jiliang>200</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>BWSN</szm>
  </Table1>
  <Table1>
    <name>卡马西平</name>
    <yongfa>口服</yongfa>
    <jiliang>100</jiliang>
    <unit>mg</unit>
    <cishu>每日1次</cishu>
    <ci>1</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>KMXP</szm>
  </Table1>
  <Table1>
    <name>盐酸苯海索</name>
    <yongfa>口服</yongfa>
    <jiliang>4</jiliang>
    <unit>mg</unit>
    <cishu>每日3次</cishu>
    <ci>3</ci>
    <jixing>片剂</jixing>
    <factory />
    <leixing>重性精神病</leixing>
    <szm>YSBHS</szm>
  </Table1>
  <Table1>
    <name>头孢</name>
    <yongfa>口服</yongfa>
    <jiliang>2</jiliang>
    <unit>片</unit>
    <cishu>每日3次</cishu>
    <ci>2</ci>
    <jixing>片</jixing>
    <leixing>高血压</leixing>
    <szm>TB</szm>
  </Table1>
</NewDataSet>`) as any
const data = JSON.parse(result).elements[0].elements;
const formatResult = data.map(item=>{
  const returnItem:any = {}
  item.elements.forEach(dd=>{
    console.log(dd)
    if(dd.elements){
      returnItem[dd.name] = dd.elements[0].text
    }
  })
  return returnItem;
})
console.log(JSON.stringify(formatResult))
export default result;