import {IBloodPressCloud} from "@/model/TerminalType";


export interface IPagination {
  currentPage: number;
  pageSize: number;
  total: number;
}

export interface IBloodPressureTableProps {
  tableData?: Array<Partial<IBloodPressCloud>>;
  pagination: IPagination;
}
export interface IHW700TableProps {
  tableData?: Array<IHW700>;
  pagination: IPagination;
}

export interface IBluetoothDesc {
  type: "success" | "warning" | "info" | "error";
  info: string;
}

export interface ISearchParams {
  measureTime: [number, number];
  name: string;
}

export interface ICheckArticle {
  name: string;
  id: string;
  type: "" | "success" | "warning" | "info" | "danger";
}

export interface IBluetooth {
  deviceId: string;
  deviceName: string;
}

/**身高体重表 */
export interface IHW700 {
  id?: number;
  data_id?: number;
  qr_code_id?: number;
  /**身份证 */
  cardId?: string;
  /**用户名 */
  userName?: string;
  /**项目 */
  article?: string;
  avatar?: string;
  /**性别 */
  sex?: string;
  measureTime?: number;
  height?: number;
  weight?: number;
  bim?: number;
  waistline?: number;
  synchronous?: number;
}

export const createHW700 = (): IHW700 => {
  return {};
};
