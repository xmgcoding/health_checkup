import { reactive, watch } from "vue"
import { every } from "lodash-es"

export default (formModel) => {
  const formatNumber = (number?: number) => {
    if (number) {
      return number
    }
    return 0
  }
  const reverseNumber = (number?: number) => {
    if (number) {
      return 6 - number
    }
    return 0
  }
  const getSumResult = (number) => {
    if (!number) {
      return ""
    }
    if (number >= 11) {
      return "1"
    } else if (number >= 9) {
      return "2"
    }
    return "0"
  }
  //气虚质
  watch(
    [
      () => formModel.b04_053_08_2,
      () => formModel.b04_053_08_3,
      () => formModel.b04_053_08_4,
      () => formModel.b04_053_08_14
    ],
    () => {
      formModel.b04_053_09 =
        formatNumber(formModel.b04_053_08_2) +
        formatNumber(formModel.b04_053_08_3) +
        formatNumber(formModel.b04_053_08_4) +
        formatNumber(formModel.b04_053_08_14)
      formModel.b04_053_18 = getSumResult(formModel.b04_053_09)
      formModel.b04_053_27 = formModel.b04_053_09 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )
  //阳虚质
  watch(
    [
      () => formModel.b04_053_08_11,
      () => formModel.b04_053_08_12,
      () => formModel.b04_053_08_13,
      () => formModel.b04_053_08_29
    ],
    () => {
      formModel.b04_053_10 =
        formatNumber(formModel.b04_053_08_11) +
        formatNumber(formModel.b04_053_08_12) +
        formatNumber(formModel.b04_053_08_13) +
        formatNumber(formModel.b04_053_08_29)
      formModel.b04_053_19 = getSumResult(formModel.b04_053_10)
      formModel.b04_053_28 = formModel.b04_053_10 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )

  //阴虚质
  watch(
    [
      () => formModel.b04_053_08_10,
      () => formModel.b04_053_08_21,
      () => formModel.b04_053_08_26,
      () => formModel.b04_053_08_31
    ],
    () => {
      formModel.b04_053_11 =
        formatNumber(formModel.b04_053_08_11) +
        formatNumber(formModel.b04_053_08_12) +
        formatNumber(formModel.b04_053_08_13) +
        formatNumber(formModel.b04_053_08_29)
      formModel.b04_053_20 = getSumResult(formModel.b04_053_11)
      formModel.b04_053_29 = formModel.b04_053_11 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )
  //痰湿质
  watch(
    [
      () => formModel.b04_053_08_9,
      () => formModel.b04_053_08_16,
      () => formModel.b04_053_08_28,
      () => formModel.b04_053_08_32
    ],
    () => {
      formModel.b04_053_12 =
        formatNumber(formModel.b04_053_08_9) +
        formatNumber(formModel.b04_053_08_16) +
        formatNumber(formModel.b04_053_08_28) +
        formatNumber(formModel.b04_053_08_32)
      formModel.b04_053_21 = getSumResult(formModel.b04_053_12)
      formModel.b04_053_30 = formModel.b04_053_12 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )

  //湿热质
  watch(
    [
      () => formModel.b04_053_08_23,
      () => formModel.b04_053_08_25,
      () => formModel.b04_053_08_27,
      () => formModel.b04_053_08_30
    ],
    () => {
      formModel.b04_053_13 =
        formatNumber(formModel.b04_053_08_23) +
        formatNumber(formModel.b04_053_08_25) +
        formatNumber(formModel.b04_053_08_27) +
        formatNumber(formModel.b04_053_08_30)
      formModel.b04_053_22 = getSumResult(formModel.b04_053_13)
      formModel.b04_053_31 = formModel.b04_053_13 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )

  //血瘀质

  watch(
    [
      () => formModel.b04_053_08_19,
      () => formModel.b04_053_08_22,
      () => formModel.b04_053_08_24,
      () => formModel.b04_053_08_33
    ],
    () => {
      formModel.b04_053_14 =
        formatNumber(formModel.b04_053_08_19) +
        formatNumber(formModel.b04_053_08_22) +
        formatNumber(formModel.b04_053_08_24) +
        formatNumber(formModel.b04_053_08_33)
      formModel.b04_053_23 = getSumResult(formModel.b04_053_14)
      formModel.b04_053_32 = formModel.b04_053_14 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )

  //气郁质

  watch(
    [
      () => formModel.b04_053_08_5,
      () => formModel.b04_053_08_6,
      () => formModel.b04_053_08_7,
      () => formModel.b04_053_08_8
    ],
    () => {
      formModel.b04_053_15 =
        formatNumber(formModel.b04_053_08_5) +
        formatNumber(formModel.b04_053_08_6) +
        formatNumber(formModel.b04_053_08_7) +
        formatNumber(formModel.b04_053_08_8)
      formModel.b04_053_24 = getSumResult(formModel.b04_053_15)
      formModel.b04_053_33 = formModel.b04_053_15 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )

  //特禀质
  watch(
    [
      () => formModel.b04_053_08_15,
      () => formModel.b04_053_08_17,
      () => formModel.b04_053_08_18,
      () => formModel.b04_053_08_20
    ],
    () => {
      formModel.b04_053_16 =
        formatNumber(formModel.b04_053_08_5) +
        formatNumber(formModel.b04_053_08_6) +
        formatNumber(formModel.b04_053_08_7) +
        formatNumber(formModel.b04_053_08_8)
      formModel.b04_053_25 = getSumResult(formModel.b04_053_16)
      formModel.b04_053_34 = formModel.b04_053_16 >= 9 ? ["1", "2", "3", "4", "5"] : []
    }
  )

  //平和质
  function checkOthers(callback) {
    const dataList = [
      formModel.b04_053_09,
      formModel.b04_053_10,
      formModel.b04_053_11,
      formModel.b04_053_12,
      formModel.b04_053_13,
      formModel.b04_053_14,
      formModel.b04_053_15,
      formModel.b04_053_16
    ]
    return every(dataList, callback)
  }

  watch(
    [
      () => formModel.b04_053_08_1,
      () => formModel.b04_053_08_2,
      () => formModel.b04_053_08_4,
      () => formModel.b04_053_08_5,
      () => formModel.b04_053_08_13
    ],
    () => {
      formModel.b04_053_17 =
        formatNumber(formModel.b04_053_08_1) +
        reverseNumber(formModel.b04_053_08_2) +
        reverseNumber(formModel.b04_053_08_4) +
        reverseNumber(formModel.b04_053_08_5) +
        reverseNumber(formModel.b04_053_08_13)
      formModel.b04_053_26 = getSumResult(formModel.b04_053_17)
      formModel.b04_053_35 = []
      if (formModel.b04_053_17 >= 17) {
        if (checkOthers((item) => item.value <= 8)) {
          formModel.b04_053_26 = "1"
        } else if (checkOthers((item) => item.value <= 10)) {
          formModel.b04_053_26 = "2"
          formModel.b04_053_35 = ["1", "2", "3", "4", "5"]
        } else {
          formModel.b04_053_26 = "0"
        }
      }
    }
  )

  const tableList = reactive([
    { label: "(1)您精力充沛吗?(指精神头足、乐于做事)", field: "b04_053_08_1" },
    { label: "(2)您容易疲乏吗?(指体力如何，是否稍微活动一下或做一点家务劳动就感到累)", field: "b04_053_08_2" },
    { label: "(3)您容易气短，呼吸短促，接不上气吗?", field: "b04_053_08_3" },
    { label: "(4)您说话声音低弱无力吗?(指说话没有力气)", field: "b04_053_08_4" },
    { label: "(5)您感到闷闷不乐、情绪低沉吗?(指心情不愉快，情绪低落)", field: "b04_053_08_5" },
    { label: "(6)您容易精神紧张、焦虑不安吗?(指遇事是否心情紧张)", field: "b04_053_08_6" },
    { label: "(7)您因为生活状态改变而感到孤独、失落吗?", field: "b04_053_08_7" },
    { label: "(8)您容易感到害怕或受到惊吓吗?", field: "b04_053_08_8" },
    { label: "(9)您感到身体超重不轻松吗?(感觉身体沉重) [BMI指数=体重(kg)/身高2(m)]", field: "b04_053_08_9" },
    { label: "(10)您眼睛干涩吗?", field: "b04_053_08_10" },
    { label: "(11)您手脚发凉吗?(不包含因周围温度低或穿的少导致的手脚发冷)", field: "b04_053_08_11" },
    {
      label: "(12)您胃脘部、背部或腰膝部怕冷吗?(指上腹部、背部、腰部或膝关节等，有一处或多处怕冷)",
      field: "b04_053_08_12"
    },
    { label: "(13)您比一般人耐受不了寒冷吗?(指比别人容易害怕冬天或是夏天的冷空调、电扇等)", field: "b04_053_08_13" },
    { label: "(14)您容易患感冒吗?(指每年感冒的次数)", field: "b04_053_08_14" },
    { label: "(15)您没有感冒时也会鼻塞、流鼻涕吗?", field: "b04_053_08_15" },
    { label: "(16)您有口粘口腻，或睡眠打鼾吗?", field: "b04_053_08_16" },
    { label: "(17)您容易过敏(对药物、食物、气味、花粉或在季节交替、气候变化时)吗?", field: "b04_053_08_17" },
    { label: "(18)您的皮肤容易起荨麻疹吗? (包括风团、风疹块、风疙瘩)", field: "b04_053_08_18" },
    {
      label: "(19)您的皮肤在不知不觉中会出现青紫瘀斑、皮下出血吗?(指皮肤在没有外伤的情况下出现青一块紫一块的情况)",
      field: "b04_053_08_19"
    },
    { label: "(20)您的皮肤一抓就红，并出现抓痕吗?(指被指甲或钝物划过后皮肤的反应)", field: "b04_053_08_20" },
    { label: "(21)您皮肤或口唇干吗?", field: "b04_053_08_21" },
    { label: "(22)您有肢体麻木或固定部位疼痛的感觉吗?", field: "b04_053_08_22" },
    { label: "(23)您面部或鼻部有油腻感或者油亮发光吗?(指脸上或鼻子)", field: "b04_053_08_23" },
    { label: "(24)您面色或目眶晦黯，或出现褐色斑块/斑点吗?", field: "b04_053_08_24" },
    { label: "(25)您有皮肤湿疹、疮疖吗?", field: "b04_053_08_25" },
    { label: "(26)您感到口干咽燥、总想喝水吗?", field: "b04_053_08_26" },
    { label: "(27)您感到口苦或嘴里有异味吗?(指口苦或口臭)", field: "b04_053_08_27" },
    { label: "(28)您腹部肥大吗?(指腹部脂肪肥厚)", field: "b04_053_08_28" },
    {
      label: "(29)您吃(喝)凉的东西会感到不舒服或者怕吃(喝)凉的东西吗?(指不喜欢吃凉的食物，或吃了凉的食物后会不舒服)",
      field: "b04_053_08_29"
    },
    { label: "(30)您有大便黏滞不爽、解不尽的感觉吗?(大便容易粘在马桶或便坑壁上)", field: "b04_053_08_30" },
    { label: "(31)您容易大便干燥吗?", field: "b04_053_08_31" },
    { label: "(32)您舌苔厚腻或有舌苔厚厚的感觉吗?(如果自我感觉不清楚可由调查员观察后填写)", field: "b04_053_08_32" },
    { label: "(33)您舌下静脉瘀紫或增粗吗?(可由调查员辅助观察后填写)", field: "b04_053_08_33" }
  ])

  const resultList = reactive([
    {
      sumField: "b04_053_09",
      name: "气虚质",
      resultField: "b04_053_18",
      adviseField: "b04_053_27",
      otherField: "b04_053_36"
    },
    {
      sumField: "b04_053_10",
      name: "阳虚质",
      resultField: "b04_053_19",
      adviseField: "b04_053_28",
      otherField: "b04_053_37"
    },
    {
      sumField: "b04_053_11",
      name: "阴虚质",
      resultField: "b04_053_20",
      adviseField: "b04_053_29",
      otherField: "b04_053_38"
    },
    {
      sumField: "b04_053_12",
      name: "痰湿质",
      resultField: "b04_053_21",
      adviseField: "b04_053_30",
      otherField: "b04_053_39"
    },
    {
      sumField: "b04_053_13",
      name: "湿热质",
      resultField: "b04_053_22",
      adviseField: "b04_053_31",
      otherField: "b04_053_40"
    },
    {
      sumField: "b04_053_14",
      name: "血瘀质",
      resultField: "b04_053_23",
      adviseField: "b04_053_32",
      otherField: "b04_053_41"
    },
    {
      sumField: "b04_053_15",
      name: "气郁质",
      resultField: "b04_053_24",
      adviseField: "b04_053_33",
      otherField: "b04_053_42"
    },
    {
      sumField: "b04_053_16",
      name: "特禀质",
      resultField: "b04_053_25",
      adviseField: "b04_053_34",
      otherField: "b04_053_43"
    },
    {
      sumField: "b04_053_17",
      name: "平和质",
      resultField: "b04_053_26",
      adviseField: "b04_053_35",
      otherField: "b04_053_44"
    }
  ])
  return {
    tableList,
    resultList
  }
}
