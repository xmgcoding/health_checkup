import { onMounted, reactive, ref } from "vue"
import { getCommunityList, getDoctorsList } from "@/api/commonApi"
export const useDoctorSelect = () => {
  const doctorsList = reactive<Array<any>>([])
  const doctorsSelectLoading = ref(false)

  onMounted(async () => {
    doctorsSelectLoading.value = true
    try {
      const data = await getDoctorsList()
      Object.assign(doctorsList, data)
    } catch (e) {
      console.log(e)
    } finally {
      doctorsSelectLoading.value = false
    }
  })
  return {
    doctorsList,
    doctorsSelectLoading
  }
}

export const useCommunitySelect = () => {
  const communityOptions = reactive<Array<any>>([])
  const communityOptionsLoading = ref(false)
  //获取医生数据
  onMounted(async () => {
    communityOptionsLoading.value = true
    try {
      const data = await getCommunityList()
      Object.assign(communityOptions, data)
    } catch (e) {
      console.log(e)
    } finally {
      communityOptionsLoading.value = false
    }
  })
  return {
    communityOptions,
    communityOptionsLoading
  }
}
// }
// const useCommonApi = () => {
//   const doctorsList = reactive<Array<any>>([])
//   const communityOptions = reactive([])
//   //获取社区数据
//   const getCommunityOptions = async () => {
//     const data = (await getCommunityList()) as any
//     Object.assign(communityOptions, data)
//   }
//   //获取医生数据
//   const getDoctorSelect = async () => {
//     try {
//       const data = await getDoctorsList()
//       Object.assign(doctorsList, data)
//     } catch (e) {
//       console.log(e)
//     }
//   }
//   return {
//     getCommunityOptions,
//     getDoctorSelect,
//     doctorsList,
//     communityOptions
//   }
// }

// export default useCommonApi
