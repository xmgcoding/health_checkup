import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
import "./samples/node-api";
import { loadPlugins } from "./plugins";
import router from "./router";
import MyComponent from "@/components";
import Dialog from "./components/DialogServer/Dialog";
import connectSocket from "./utils/SingleWS";

// import xmlToJson from "@/config/xmlToJson";
// console.log(xmlToJson)
const app = createApp(App);
app.use(Dialog);
console.log("MODE", import.meta.env.MODE);
// 连接socket
if (import.meta.env.MODE === "web_terminal") {
  connectSocket();
}
loadPlugins(app);
app.use(MyComponent);
app.use(router);
app
  .mount("#app")
  .$nextTick(() => {
    postMessage({ payload: "removeLoading" }, "*");
  })
  .then((r) => console.log(r));
