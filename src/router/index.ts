import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
export const webRoute = {
  path: "/web",
  name: "web",
  component: () => import("@/Layout/index.vue"),
  redirect: "/web/archive",
  children: [
    {
      path: "archive",
      component: () => import("@/views/Archive/select.vue"),
      meta: {
        title: "档案信息",
      },
    },
    {
      path: "check_body",
      component: () => import("@/views/CheckBody/index.vue"),
      meta: {
        title: "体检信息",
      },
    },
    {
      path: "qrCode",
      component: () => import("@/views/QrCode/index.vue"),
      meta: {
        title: "二维码",
      },
    }, {
      path: "checkStatus",
      component: () => import("@/views/CheckStatus/index.vue"),
      meta: {
        title: "体检状态",
      },
    }
  ],
};
export const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    redirect: (to) => {
      console.log(import.meta.env.VITE_APP_ROUTER);
      return {
        name: import.meta.env.VITE_APP_ROUTER,
      };
    },
  },
  webRoute,
  {
    path: "/agedTcm",
    name: "agedTcm",
    component: () => import("@/views/AgedTcm/index.vue"),
  },
  {
    path: "/biochemical",
    name: "biochemical",
    component: () => import("@/views/Biochemical/index.vue"),
  },
  {
    path: "/hw700",
    name: "hw700",
    component: () => import("@/views/HW700/index.vue"),
  },
  {
    path: "/blood",
    name: "blood",
    component: () => import("@/views/Blood/index.vue"),
  },
  {
    path: "/healthCheck",
    name: "healthCheck",
    component: () => import("@/views/HealthCheck/index.vue"),
  },
  {
    path: "/urinalysis_terminal",
    name: "urinalysis_terminal",
    component: () => import("@/views/Urinalysis/index.vue"),
  },
  {
    path: "/bus_view",
    name: "bus_view",
    component: () => import("@/views/BusView/index.vue"),
  },
   {
    path: "/biochemistry",
     name: "biochemistry",
    component: () => import("@/views/Biochemistry/index.vue"),
    
  },
];
const router = createRouter({
  history: createWebHashHistory("/"),
  routes,
});

export default router;
