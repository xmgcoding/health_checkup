import { ICardReaderContent } from "@/model";
import { ref } from "vue";
const doctorUser = ref<Partial<ICardReaderContent>>();
const doctorUserLogout = () => {
  doctorUser.value = {};
};
const doctorUserLogin = (info: ICardReaderContent) => {
  doctorUser.value = { ...info };
};
export default () => {
  return { doctorUser, doctorUserLogout, doctorUserLogin };
};
