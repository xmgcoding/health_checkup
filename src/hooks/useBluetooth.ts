import { IBluetooth, IBluetoothDesc } from "@/model/BloodType";
import { ipcRenderer } from "electron";
import { reactive, ref } from "vue";

export default function useBluetooth() {
  // 缓存的设备，刚登陆系统选择一次设备
  const cacheDeviceId = ref("");
  const bluetoothList = ref<Array<IBluetooth>>();

  // 蓝牙状态描述
  const bluetoothDesc = ref<IBluetoothDesc>({
    type: "success",
    info:
      "蓝牙未连接，请点击开始测量按钮！" +
      (cacheDeviceId.value !== "" ? "(已缓存设备)" : "(未缓存设备)"),
  });

  // 弹框选择蓝牙
  const bluetoothDialog = reactive({
    visible: false,
    cacheDeviceId: cacheDeviceId.value,
    handleClose: function () {
      bluetoothDesc.value.info = "蓝牙未连接，请点击开始测量按钮！";
      bluetoothDialog.visible = false;
    },
    handleOk() {
      bluetoothDialog.visible = false;
      ipcRenderer.send("bluetooth-select-done", bluetoothDialog.cacheDeviceId);
      bluetoothList.value = [];
      cacheDeviceId.value = bluetoothDialog.cacheDeviceId;
    },
  });
}
