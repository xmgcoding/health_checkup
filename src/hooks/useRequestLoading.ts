import { ref } from "vue";

export default async (request) => {
  const asyncStatus = ref(false);
  const asyncData = ref<any>();
  try {
    asyncData.value = await request();
  } catch (e) {
  } finally {
    asyncStatus.value = true;
  }
  return { asyncStatus, asyncData };
};
