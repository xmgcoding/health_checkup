import { ICardReaderContent } from "@/model";
import { ref } from "vue";
const medicalUser = ref<Partial<ICardReaderContent>>();
const medicalUserLogout = () => {
  medicalUser.value = {};
};
const medicalUserLogin = (info: ICardReaderContent) => {
  medicalUser.value = { ...info };
};
export default () => {
  return { medicalUser, medicalUserLogout, medicalUserLogin };
};
