import { ref } from "vue";

export const loginModelStatus = ref(false);

export const useLoginModelStatus = () => {
  const setLoginModelStatus = (status: boolean) => {
    loginModelStatus.value = status;
  };
  return {
    loginModelStatus,
  };
};
