import {Table} from "dexie";
import {reactive} from "vue";
import {ITerminalB04_53} from "@/model";
import {db, pageLocalRequest} from "@/db/db";
import useMyDialog from "@/components/MyDialog/useMyDialog";
import dayjs from "dayjs";

export default (table: Table,proTable,editRow:Function) => {
 
  const refresh = () => {
    if (proTable.value) {
      proTable.value.refresh();
    }
  };
  const handleHistoryClick = () => {
    historyDialog.visible = true
  }
  const historyDialog = useMyDialog({
    visible: false,
    title: "历史数据",
  });
  const historyTableProps = reactive({
    request: (searchItem) => {
      console.log(searchItem);
      if (searchItem.measureTimeStart && searchItem.measureTimeEnd) {
        searchItem.measureTime = [dayjs(searchItem.measureTimeStart).startOf("d").valueOf(), dayjs(searchItem.measureTimeEnd).endOf('d').valueOf()]
      }
      return pageLocalRequest(table, searchItem);
    },
    columns: [
      {label: "姓名", prop: "userName"},
      {label: "身份证号", prop: "cardId"},
      {label: "检查项目", prop: "article"},
      {label: "检查时间", prop: "measureTime", tdSlot: "measureTime"},
      {
        label: "是否同步",
        prop: "synchronous",
        tdSlot: "synchronous",
      },
      {
        label: "操作",
        fixed: "right",
        width: 180,
        align: "center",
        tdSlot: "operate", // 自定义单元格内容的插槽名称
      },
    ],
    search: {
      fields: [
        {
          label: "时间",
          name: "measureTime",
          type: "daterange",
          trueNames: ["measureTimeStart", "measureTimeEnd"],
          style: {width: "360px"},
        }, {
          label: "姓名/身份证号",
          name: "name",
          type: "text"
        }
      ]
    }
  });

  const handleHistoryEdit = (row: any) => {
    editRow(row)
    historyDialog.visible = false
  }
  const handleHistoryDel = async (row: ITerminalB04_53) => {
    if (row.id) {
      await table.delete(row.id)
      refresh()
    }
  }

  return {
    handleHistoryEdit,
    handleHistoryDel,
    historyTableProps,
    historyDialog,
    refresh,
    handleHistoryClick
  }
}