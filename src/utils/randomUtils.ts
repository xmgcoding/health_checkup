export function randomRound(max, min) {
  return Math.round((Math.random() * (max - min) + min) * 10) / 10;
}
