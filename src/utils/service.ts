import { Request } from "./request"

export { type IPage } from "./request"
export const service = new Request({
    baseURL: import.meta.env.VITE_BASE_API
})
