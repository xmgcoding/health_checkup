export const objRemoveNull = (obj) => {
  Object.keys(obj).forEach((item) => {
    if (obj[item] === null || obj[item] === undefined) {
      delete obj[item];
    }
  });
  return obj;
};
