import { ipcRenderer } from "electron/renderer";
// 5.收到主进程可更新的消息
ipcRenderer.send("updateAvailable", (event, data) => {
  // 6. 点击确认更新
  ipcRenderer.send("confirmUpdate");
});

// 9. 收到进度信息，做进度条
ipcRenderer.on("downloadProgress", (event, data) => {
  // do sth.
});

// 11. 下载完成，反馈给用户是否立即更新
ipcRenderer.on("updateDownloaded", (event, data) => {
  // do sth.
  // 12. 告诉主进程，立即更新
  ipcRenderer.send("updateNow");
});
