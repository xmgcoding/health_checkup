export const getBMI = (height: number, weight: number) => {
  const BIM = weight / ((height * height) / (100 * 100))
  return Math.round(BIM * 10) / 10
}

export const checkBMI = (BIM: number) => {
  const line1 = 18.5

  const line2 = 23.9

  const line3 = 27.9

  const line4 = 31.9

  if (BIM < line1) {
    return "偏瘦"
  } else if (BIM >= line1 && BIM <= line2) {
    return "正常"
  } else if (BIM > line2 && BIM < line3) {
    return "过重"
  } else if (BIM >= line3 && BIM < line4) {
    return "肥胖"
  } else if (BIM >= line4) {
    return "非常肥胖"
  }
}

export const getWaistline = (height: number, sex?: string) => {
  if (sex === "男") {
    return height / 2 - 11
  } else {
    return height / 2 - 14
  }
}
