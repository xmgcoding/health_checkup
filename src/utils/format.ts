import dayjs from "dayjs";

export const dateTimeFormatter = (data)=>{
  if(!data){
    return ""
  }
  return dayjs(data).format("YYYY-MM-DD HH:mm:ss")
}