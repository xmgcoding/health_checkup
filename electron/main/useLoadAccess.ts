import * as odbc from "odbc";
import {BrowserWindow, ipcMain} from "electron";

export default (win: BrowserWindow) => {
  ipcMain.on("load-access-data", async (event, config) => {
    const queryString = `select Pat_Name,Pat_Sex,Pat_ID,Pat_TestNo,Pat_Sending_Date,
 Pat_Testing_Date,Pat_Report_Date,Pat_Test_Time,Pat_Doctor,Pat_Checker from Dr_patient where Pat_id>=13000 and  Pat_Sending_Date=#${config.queryDate}#`
    
    const dbInfo = "DSN=" + config.DSN
    console.log(config)
    try {
      const connect = await odbc.connect(dbInfo);
      const data:Array<any> = await connect.query(queryString)
      for (let item of data) {
        const queryItemString = `select 
            DR_Test_App.TestName, 
            DR_Test_App.TestCnName ,
            DR_Test_App.TestResult,
             DR_Test_App.TestRange,
             DR_Test_App.TestUnit 
             from Dr_patient  
             left join DR_Test_App 
             on 
             Dr_Patient.Pat_TestNo=DR_Test_App.TestNo 
             and 
             Dr_Patient.Pat_Sending_Date=DR_Test_App.TestDate  
             where 
             Pat_id=${item.Pat_ID} 
             order by 
             DR_Test_App.ID asc`
        try {
          item.result = await connect.query(queryItemString);
          console.log("result",item.result)
        }catch (e) {
          console.log(2333,e)
        }
      }
      win.webContents.send("send-access-data", data)
    } catch (e) {
      console.log(222,e)
      win.webContents.send("send-access-data", [])
    }
  })
}