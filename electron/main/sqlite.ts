import { open } from "sqlite";
import sqlite3 from "sqlite3";
import dayjs from "dayjs";
export async function openDB(path) {
  try {
    const db = await open({
      filename: path,
      driver: sqlite3.Database,
    });
    return db;
  } catch (error) {
    return null;
  }
}

export async function getRow(config) {
  try {
    const db = await openDB(config.path);
    if (db) {
      const queryTimeStart = config.queryDate;
      const queryTimeEnd = dayjs(config.queryDate)
        .add(1, "days")
        .format("YYYY-MM-DD");
      console.log(queryTimeStart, queryTimeEnd);
      const result = await db.all(`select * from TestResult 
      left join TestInfo on  TestInfo.id=TestResult.TestInfo    
      left join NormalResult on    TestResult.NormalResult=NormalResult.id
      where TestResult.Time>"${queryTimeStart}" and TestResult.Time<="${queryTimeEnd}"  and TestResult.state=2
      order by TestResult.ID asc`);
      return result;
    }
    return null;
  } catch (error) {
    console.log(error);
  }
}
