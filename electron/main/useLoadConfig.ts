import fs from "fs";
import { app, BrowserWindow, ipcMain } from "electron";
import log from "electron-log";
import * as path from "path";
export default (win: BrowserWindow) => {
  ipcMain.on("get-config-file", () => {
    let url = "";
    if (process.env.VITE_DEV_SERVER_URL) {
      url = "./config.json";
    } else {
      url = path.dirname(app.getPath("exe")) + "\\config.json";
    }
    log.info("config.file", url);
    fs.readFile(url, "utf-8", (err, data) => {
      let config = "";
      console.log(11, data);
      if (data) {
        config = JSON.parse(data);
      }
      win.webContents.send("config-file-done", config);
    });
  });
};
