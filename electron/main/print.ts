import {
  PosPrintData,
  PosPrinter,
  PosPrintOptions,
} from "electron-pos-printer";

export const handlePrint = (e, printConfig) => {
  const printData = printConfig.printData;
  const options: PosPrintOptions = {
    boolean: "",
    // 静默打印
    silent: true,
    preview: false,
    margin: "auto",
    copies: printConfig.copies,
    timeOutPerLine: 40000,
    pageSize: {
      width: 150,
      height: 115,
    },
  };
  const data: PosPrintData[] = [];
  function stringToBase64(str) {
    const bb = Buffer.from(str);
    return bb.toString("base64");
  }

  data.push({
    type: "text",
    value: `${printData.username} ${printData.sex} ${printData.age} ${printData.lgt}`,
    style: { textAlign: "center", fontSize: "8px", marginTop: "-5px" },
  });
  data.push({
    type: "qrCode",
    value: JSON.stringify(printData),
    height: "90",
    width: "90",
    position: "center",
    style: { marginTop: "-10px", marginBottom: "0px" },
  });
  data.push({
    type: "text",
    value: `${printData.idno}`,
    style: { textAlign: "center", fontSize: "8px", top: "-5px" },
  });
  try {
    PosPrinter.print(data, options)
      .then(() => console.log("done"))
      .catch((error) => {
        console.log(11111);
        console.error(error);
      });
  } catch (error) {
    console.log(PosPrinter);
    console.log(error);
  }
};
