import { app, BrowserWindow, shell, ipcMain, Menu, screen } from "electron";
import { release } from "node:os";
import { join } from "node:path";
import log from "electron-log";
import { getRow } from "./sqlite";
import { handlePrint } from "./print";
import useLoadConfig from "./useLoadConfig";
import useLoadAccess from "./useLoadAccess";
import useLoadPrintPdf from "./useLoadPrintPdf";
import useLoadMssql from "./useLoadMssql";
console.log(process.version);
// import { msCon } from "./mssql";
// msCon();
// import { PosPrinter } from "electron-pos-printer";

// The built directory structure
//
// ├─┬ dist-electron
// │ ├─┬ main
// │ │ └── index.js    > Electron-Main
// │ └─┬ preload
// │   └── index.js    > Preload-Scripts
// ├─┬ dist
// │ └── index.html    > Electron-Renderer
//
process.env.DIST_ELECTRON = join(__dirname, "..");
process.env.DIST = join(process.env.DIST_ELECTRON, "../dist");
process.env.PUBLIC = process.env.VITE_DEV_SERVER_URL
  ? join(process.env.DIST_ELECTRON, "../public")
  : process.env.DIST;

// Disable GPU Acceleration for Windows 7
if (release().startsWith("6.1")) app.disableHardwareAcceleration();

// Set application name for Windows 10+ notifications
if (process.platform === "win32") app.setAppUserModelId(app.getName());

if (!app.requestSingleInstanceLock()) {
  app.quit();
  process.exit(0);
}
Menu.setApplicationMenu(null);

// Remove electron security warnings
// This warning only shows in development mode
// Read more on https://www.electronjs.org/docs/latest/tutorial/security
// process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true'

let win: BrowserWindow | null = null;
// Here, you can also use other preload
const preload = join(__dirname, "../preload/index.js");
const url = process.env.VITE_DEV_SERVER_URL;
const indexHtml = join(process.env.DIST, "index.html");
log.info("indexHtml", indexHtml);
log.info("DIST", process.env.DIST);
log.info("PUBLIC", process.env.PUBLIC);
log.info("VITE_DEV_SERVER_URL", process.env.VITE_DEV_SERVER_URL);
let callbackForBluetoothEvent = null;

async function createWindow() {
  const primaryDisplay = screen.getPrimaryDisplay();
  const { width, height } = primaryDisplay.workAreaSize;
  win = new BrowserWindow({
    // title: "Main window",
    icon: join(process.env.PUBLIC, "logo.ico"),
    height: import.meta.env.VITE_APP_HEIGHT
      ? Number(import.meta.env.VITE_APP_HEIGHT)
      : height,
    width: import.meta.env.VITE_APP_WIDTH
      ? Number(import.meta.env.VITE_APP_WIDTH)
      : width,
    webPreferences: {
      preload,
      // Warning: Enable nodeIntegration and disable contextIsolation is not secure in production
      // Consider using contextBridge.exposeInMainWorld
      // Read more on https://www.electronjs.org/docs/latest/tutorial/context-isolation
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  if (process.env.VITE_DEV_SERVER_URL) {
    // electron-vite-vue#298
    win.loadURL(url);
    // Open devTool if the app is not packaged
    win.webContents.openDevTools();
  } else {
    win.loadFile(indexHtml);
  }

  // Test actively push message to the Electron-Renderer
  win.webContents.on("did-finish-load", () => {
    win?.webContents.send("main-process-message", new Date().toLocaleString());
  });

  // Make all links open with the browser, not with the application
  win.webContents.setWindowOpenHandler(({ url }) => {
    if (url.startsWith("https:")) shell.openExternal(url);
    return { action: "deny" };
  });

  if (process.platform === "linux") {
    app.commandLine.appendSwitch(
      "enable-experimental-web-platform-features",
      "true"
    );
  } else {
    app.commandLine.appendSwitch("enable-web-bluetooth");
  }
  win.webContents.on(
    "select-bluetooth-device",
    (event, deviceList, callback) => {
      event.preventDefault(); //important, otherwise first available device will be selected
      callbackForBluetoothEvent = callback; //to make it accessible outside createWindow()
      // console.log(deviceList)
      win.webContents.send("send-bluetooth-device-list", deviceList);
      // console.log("Bluetooth device list dispatched.")
    }
  );

  // Listen for a message from the renderer to get the response for the Bluetooth pairing.
  // ipcMain.on('bluetooth-pairing-response', (event, response) => {
  //   console.log(2)
  //   bluetoothPinCallback(response)
  // })

  // win.webContents.session.setBluetoothPairingHandler((details, callback) => {
  //
  //   console.log(1)
  //   bluetoothPinCallback = callback
  //   // Send a message to the renderer to prompt the user to confirm the pairing.
  //   win.webContents.send('bluetooth-pairing-request', details)
  // })

  useLoadConfig(win);
  useLoadAccess(win);
  useLoadPrintPdf(win);
  useLoadMssql(win);
}
console.log(app.getPath("userData"));

// Listen for an IPC message from the renderer to get the response for the Bluetooth pairing.
// ipcMain.on('bluetooth-pairing-response', (event, response) => {
//   console.log(1,response)
//   bluetoothPinCallback(response)
// })

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
  win = null;
  if (process.platform !== "darwin") app.quit();
});
log.info("appName", app.getName());
app.on("second-instance", () => {
  if (win) {
    // Focus on the main window if the user tried to open another
    if (win.isMinimized()) win.restore();
    win.focus();
  }
});

app.on("activate", () => {
  const allWindows = BrowserWindow.getAllWindows();
  if (allWindows.length) {
    allWindows[0].focus();
  } else {
    createWindow();
  }
});

// New window example arg: new windows url
ipcMain.handle("open-win", (_, arg) => {
  const childWindow = new BrowserWindow({
    webPreferences: {
      preload,
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  if (process.env.VITE_DEV_SERVER_URL) {
    childWindow.loadURL(`${url}#${arg}`);
  } else {
    childWindow.loadFile(indexHtml, { hash: arg });
  }
});

ipcMain.on("print-qr-code", handlePrint);
//蓝牙选择器
ipcMain.on("cancel-bluetooth-select", (event, DeviceId) => {
  if (callbackForBluetoothEvent) {
    callbackForBluetoothEvent(""); //reference to callback of win.webContents.on('select-bluetooth-device'...)
    callbackForBluetoothEvent = null;
    win.reload();
  }
});
//resolves navigator.bluetooth.requestDevice() and stops device discovery
ipcMain.on("bluetooth-select-done", (event, DeviceId) => {
  console.log("stop device discover");
  console.log(callbackForBluetoothEvent);
  if (callbackForBluetoothEvent) {
    console.log("DeviceId", DeviceId);
    callbackForBluetoothEvent(DeviceId); //reference to callback of win.webContents.on('select-bluetooth-device'...)
    callbackForBluetoothEvent = null;
  } else {
    if (!DeviceId) {
      win.reload();
    }
  }
  console.log("Device selected, discovery finished");
});

ipcMain.on("load-sqlite-data", (event, config) => {
  getRow(config).then((data) => {
    win.webContents.send("send-sqlite-data", data);
  });
});
// 通过access读取mdb
// mdb()

//获取配置文件

// 自动更新
