# 安装

```bash
npm install
```

# 运行

```bash
# 运行管理终端
npm run dev:web_terminal
```

```bash
# 运行中医体质
npm run dev:aged_tcm
```

```bash
# 运行身高体重
npm run dev:hw_terminal
```

```bash
# 运行血压
npm run dev:blood_terminal
```

```bash
# 运行体检问询
npm run dev:health_check
```
```bash
# B超
npm run dev:bus_view
```

```bash
# 血球
npm run dev:biochemical
```

